package ru.rpg.phones.process;

import java.util.Hashtable;

import ru.rpg.phones.ISerializer;
import ru.rpg.phones.InteractionModel.Descriptor;

public interface IEvent {
	public abstract Descriptor handle();

	public abstract String getName();
	public abstract Hashtable cloneData();

	public abstract void unserializeData(ISerializer ser);

	public abstract void serializeData(ISerializer ser);

}
