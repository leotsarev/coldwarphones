package ru.rpg.phones.process;

import ru.rpg.phones.InteractionModel.Descriptor;
import ru.rpg.phones.process.ProcessModelBase.Process;

public class DoNothingProcess extends Process {

	public DoNothingProcess(ProcessModelBase model) {
		super(model);
	}

	public Descriptor handle() {
		// NOP
		return null;
	}

	public String getName() {
		return "DoNothing";
	}

}
