package ru.rpg.phones.process.sample;

import ru.rpg.phones.process.ProcessModelBase;
import ru.rpg.phones.process.optional.OneTimeMessageBase;

public class HitMeProcess extends OneTimeMessageBase {

	public HitMeProcess(ProcessModelBase model) {
		super(model, "Ты ударил себя. Это больно");
	}

	public String getName() {
		return "HitMe";
	}

}
