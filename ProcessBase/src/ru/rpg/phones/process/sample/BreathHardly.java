package ru.rpg.phones.process.sample;

import ru.rpg.phones.InteractionModel.*;


public class BreathHardly extends ru.rpg.phones.process.ProcessModelBase.Event{

	public BreathHardly(ru.rpg.phones.process.ProcessModelBase model) {
		super(model);
	}

	public Descriptor handle() {
		SleepDescriptor result = new SleepDescriptor();
		result.status = "Немного не хватает воздуха. Вы тяжело дышите.";
		result.timeout = 30;
		model.scheduler().afterSecs(this, 1);
		return result;
	}

	public String getName() {
		return "BreathHardly";
	}

}
