package ru.rpg.phones.process;


public interface IPrefixHandler extends IEvent {
	void setSuffixValue(String suffix);

	boolean isValidSuffix(String suffix);

	boolean isStartOfSuffix(String suffix);
}
