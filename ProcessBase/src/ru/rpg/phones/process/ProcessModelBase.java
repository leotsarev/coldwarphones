package ru.rpg.phones.process;

import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;

import ru.rpg.phones.ISerializer;
import ru.rpg.phones.InteractionModel;
import ru.rpg.phones.PhoneWordTransformer;
import ru.rpg.phones.Utils;

public class ProcessModelBase extends InteractionModel{
	
	private static final int INFINITE_SLEEP = 60;
	private Hashtable usedCodes;
	private Vector commandWordDefs = new Vector();
	private ProcessScheduler eventScheduler = new ProcessScheduler();
	
	public IScheduler scheduler()
	{
		return eventScheduler;
	}
	
	private abstract class CommandWordDefBase
	{
		protected final IEvent processTemplate;
		protected final String param;
		
		protected CommandWordDefBase(IEvent processTemplate, String param)
		{
			this.processTemplate = processTemplate;
			this.param = param;
		}
		
		public abstract boolean isValidWord(String word);
		public abstract boolean isValidPrefix(String word);
		public abstract IEvent get(String word);
		public boolean isOneTimeCode(String code) {
			return false;
		}

		public IEvent createProcess() {
			Event process = createProcessByName(processTemplate.getName());
			Utils.assert_(process != null, "Не удалось создать процесс по имени " + processTemplate.getName());
			process.EventData = processTemplate.cloneData();
			return process;
		}

		public abstract String getName();

		public void serialize(ISerializer ser) {
			ser.writeString(getName());
			serializeProcess(ser, processTemplate);
			ser.writeString(param);
		}
		
	}
	
	private CommandWordDefBase unserializeCommandWord(ISerializer ser) {
		String name = ser.readString();
		IEvent template = unserializeProcess(ser);
		String param = ser.readString();
		
		IPrefixHandler prefixHandler = template instanceof IPrefixHandler ?  (IPrefixHandler) template : null;
		CommandWordDefBase[] cwd =
			{
				new MenuPrefixCommandWord(param, prefixHandler),
				new FixedCommandWord(param, template),
				new FixedPhoneWord(param, template)
			};
		for (int i = 0; i < cwd.length; i++)
		{
			if (name.equals(cwd[i].getName()))
			{
				return cwd[i];
			}
		}
		return null;
	}
	
	private class MenuPrefixCommandWord extends CommandWordDefBase
	{

		private final String commandWordPrefix;

		public MenuPrefixCommandWord(String commandWordPrefix, IPrefixHandler processTemplate) {
			super(processTemplate, commandWordPrefix);
			this.commandWordPrefix = commandWordPrefix;
		}

		public boolean isValidWord(String word) {
			if (!word.startsWith(commandWordPrefix))
			{
				return false;
			}
			return ((IPrefixHandler)processTemplate).isValidSuffix(getSuffix(word));
		}

		private String getSuffix(String word) {
			Utils.assert_(word.length() >= commandWordPrefix.length(), "Invalid word " + word);
			return word.substring(commandWordPrefix.length());
		}

		public boolean isValidPrefix(String word) {
			if (commandWordPrefix.startsWith(word))
			{
				return true;
			}
			if (!word.startsWith(commandWordPrefix))
			{
				return false;
			}
			return ((IPrefixHandler)processTemplate).isStartOfSuffix(getSuffix(word));
		}

		public IEvent get(String word) {
			Utils.assert_(isValidWord(word));
			IPrefixHandler process = (IPrefixHandler) super.createProcess();
			process.setSuffixValue(getSuffix(word));
			return process;
		}

		public String getName() {
			return "MenuPrefixCommandWord";
		}		
	}
	
	private class FixedCommandWord extends CommandWordDefBase
	{
		private String fixedWord;
		public FixedCommandWord(String fixedWord, IEvent processTemplate)
		{
			super(processTemplate, fixedWord);
			this.fixedWord = fixedWord;
		}
		
		public IEvent get(String word) {
			if (isValidWord(word))
			{
				IEvent process = createProcess();
				return process;
			}
			return null;
		}

		public boolean isValidWord(String word) {
			return word.equals(fixedWord);
		}

		public boolean isValidPrefix(String word) {
			return fixedWord.startsWith(word);
		}

		public String getName() {
			return "FixedCommandWord";
		}
	}
	
	class FixedPhoneWord extends FixedCommandWord
	{
		public FixedPhoneWord(String fixedWord, IEvent processTemplate) {
			super(PhoneWordTransformer.transformToPhoneWord(fixedWord), processTemplate);
		}
	}
	
	public static abstract class Event implements IEvent
	{
		public Event (ProcessModelBase model)
		{
			this.model = model;
		}
		public abstract Descriptor handle();
		private Hashtable EventData = new Hashtable();
		public abstract String getName();
		protected ProcessModelBase model;
		
		protected final String getStringArg(String key)
		{
			return (String) EventData.get(key);
		}
		
		protected final void setStringArg(String key, String value)
		{
			EventData.put(key, value);
		}
		
		protected final int getIntArg(String key) {
			return Integer.parseInt(getStringArg(key));
		}
		
		protected final void setIntArg(String key, int coord) {
			setStringArg(key, Integer.toString(coord));
		}
		
		public final void serializeData(ISerializer ser)
		{
			ser.writeDict(EventData);
		}
		
		public final void unserializeData (ISerializer ser)
		{
			EventData = ser.readDict();
		}

		public Descriptor createMessage(String message) {
			MenuDescriptor result = new MenuDescriptor();
			result.menuHeader = message;
			result.timeout = 600;
			result.alarm = ALARM_RECURRENT;
			result.addItem("Ok", "");
			return result;
		}
		
		public Descriptor createChangeStatusMessage(String message)
		{
			MenuDescriptor result = new MenuDescriptor();
			result.menuHeader = message;
			result.timeout = 10;
			result.alarm = ALARM_SILENT;
			result.addItem("Ok", "");
			return result;
		}
		
		public final Hashtable cloneData()
		{
			Enumeration keys = EventData.keys();
			Hashtable result = new Hashtable();
			while (keys.hasMoreElements())
			{
				Object key = keys.nextElement();
				result.put(key, EventData.get(key));
			}
			return result;
		}
		
		protected void addMenuItemAndBind(MenuDescriptor menu, String itemName,
				IEvent process)
		{
			if (model.checkCommandWord(process.getName()) == InteractionModel.CODE_UNKNOWN)
			{
				model.bindFixedCommandWord(process);
			}
			menu.addItem(itemName, process.getName());
		}
		
		public boolean equals(Event process) {
			return getName().equals(process.getName())  && EventData.equals(((Event)process).EventData);
		}
		
		public boolean equals(Object obj)
		{
			return obj !=null && obj instanceof Event && equals((Event)obj);
		}
		protected Descriptor nothing() {
			return createMessage("Ничего не произошло");
		}
	}
	
	private static class ScheduleItem
	{
		public ScheduleItem(IEvent ev, int time)
		{
			this.ev = ev;
			this.time = time;
			
		}
		public final IEvent ev;
		public final int time;
	}
	
	private class ProcessScheduler implements IScheduler
	{
		Vector events = new Vector();
		private int targetSec;
		private int currentSec;
		private int prevSecs;

		private void atTime(IEvent process, int time) {
			Utils.assert_(process.getName() != null);
			events.addElement(new ScheduleItem(process, time));
		}
		
		public void afterSecs(IEvent process, int secs)
		{
			Utils.assert_(process.getName() != null);
			Utils.assert_(secs >=0);
			atTime(process, secs + currentSec);
		}
		
		public void afterMins(IEvent event, int mins) {
			afterSecs(event, mins * 60);
		}
		
		public void now (IEvent event)
		{
			afterSecs(event, 0);
		}
		
		private void atTarget(IEvent process) {
			atTime(process, eventScheduler.targetSec);
		}


		private void serialize(ISerializer ser) {
			ser.writeInt(events.size());
			for (int i=0; i<events.size(); i++)
			{
				ScheduleItem scheduleItem = (ScheduleItem) events.elementAt(i);
				ser.writeInt(scheduleItem.time);
				serializeProcess(ser, scheduleItem.ev);
			}
		}

		private void unserialize(ISerializer ser) {
			int size = ser.readInt();

			for (int i = 0; i <size; i++)
			{
				int time = ser.readInt();
				IEvent ev = unserializeProcess(ser);
				events.addElement(new ScheduleItem(ev, time));
			}
		}
		
		private int getNextEventTime() {
			int result = Integer.MAX_VALUE;
			for (int i=0; i<events.size(); i++)
			{
				result = Math.min(result, ((ScheduleItem) events.elementAt(i)).time);
			}
			return result;
			
		}

		private IEvent popForTime(int time) {
			for (int i = events.size()-1; i>= 0; i--)
			{
				ScheduleItem sc = (ScheduleItem) events.elementAt(i);
				if (sc.time == time)
				{
					events.removeElement(sc);
					return sc.ev;
				}
			}
			return null;
		}
		
	}
	
	private void checkCreatePossible(IEvent process) {
		Event sampleProcess = createProcessByName(process.getName());
		Utils.assert_(sampleProcess != null, "Don't know how to create " + process.getName() + " bind failed.");
		Utils.assert_(sampleProcess.getName().equals(process.getName()), "Try create " + process.getName() + ", but got: " + sampleProcess.getName());
	}
	
	protected void bindFixedCommandWord(String commandWord, IEvent process)
	{
		checkCreatePossible(process);
		commandWordDefs.addElement(new FixedCommandWord(commandWord, process));
	}
	
	protected void bindFixedCommandWord(IEvent process) {
		bindFixedCommandWord(process.getName(), process);
	}

	protected void bindPrefixCommandWord(String commandWordPrefix, IPrefixHandler process) {
		checkCreatePossible(process);
		commandWordDefs.addElement(new MenuPrefixCommandWord(commandWordPrefix, process));
	}
	
	protected void bindFixedPhoneWord(String string, IEvent process) {
		checkCreatePossible(process);
		commandWordDefs.addElement(new FixedPhoneWord(string, process));
	}
	
	

	
	public void reset() {
		init();
	}

	private void init() {
		eventScheduler = new ProcessScheduler();
		commandWordDefs = new Vector();
		usedCodes = new Hashtable();
	}
	

	

	private String code;
	private Random random = new Random(System.currentTimeMillis());
	
	public Descriptor whatNext(int passedSecs, Date currentTime) {
		Utils.assert_(eventScheduler.prevSecs <= eventScheduler.currentSec);
		eventScheduler.prevSecs = eventScheduler.currentSec;
		
		eventScheduler.targetSec += passedSecs;
		
		if (code != null)
		{
			IEvent process = getProcessForCode(code);
			if (process != null)
			{
				eventScheduler.atTarget(process);
			}
			code = null;
		}
		
		Descriptor result = null;
		while (result == null)
		{
			advanceTime();
			IEvent process = eventScheduler.popForTime(eventScheduler.currentSec);
			if (process == null)
			{
				return sleepToNextEvent();
			}
			result = process.handle();
		}
		advanceTime();
		return result;
	}




	private void advanceTime() {
		int nextTime = Math.min(eventScheduler.targetSec, eventScheduler.getNextEventTime());
		Utils.assert_(eventScheduler.currentSec <= nextTime);
		eventScheduler.currentSec = nextTime;
	}

	private Descriptor sleepToNextEvent() {
		Utils.assert_(eventScheduler.currentSec == eventScheduler.targetSec);
		Descriptor result = new SleepDescriptor(getStatus());
		int nextEventTime = eventScheduler.getNextEventTime();
		
		result.timeout = nextEventTime == Integer.MAX_VALUE ? INFINITE_SLEEP : ( nextEventTime - eventScheduler.targetSec);
		return result;
	}

	private String getStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	public void assertCommandWord(String code) {
		this.code = code;
	}

	private IEvent getProcessForCode(String code) {
		if (code.equals(""))
		{
			return new DoNothingEvent(this);
		}
		for (int i = 0; i < commandWordDefs.size(); i++)
		{
			CommandWordDefBase def = (CommandWordDefBase) commandWordDefs.elementAt(i);
			if (def.isValidWord(code))
			{
				if (def.isOneTimeCode(code))
				{
					usedCodes.put(code, "");
				}
				return def.get(code);
			}
		}
		Utils.assert_(false, "Failed to bind code '" + code + "'");
		return null;
	}
	
	public ProcessModelBase()
	{
		init();
	}
	
	public final int checkCommandWord(String commandWord) {
		if (commandWord == "")
		{
			return CODE_VALID;
		}
		if (usedCodes.containsKey(commandWord))
		{
			return CODE_USED;
		}
		int status = super.checkCommandWord(commandWord);
		Utils.assert_(commandWordDefs.size() > 0);
		for (int i = 0; i < commandWordDefs.size(); i++)
		{
			CommandWordDefBase def = (CommandWordDefBase) commandWordDefs.elementAt(i);
			if (def.isValidWord(commandWord))
			{
				return CODE_VALID;
			}
			if (def.isValidPrefix(commandWord))
			{
				status = CODE_PREFIX;
			}
		}
		return status;
	}

	public void unserialize(ISerializer ser) {
		init();
		eventScheduler.unserialize(ser);
		usedCodes = ser.readDict();
		
		int cmdWordSize = ser.readInt();

		commandWordDefs = new Vector(cmdWordSize);
		for (int i =0; i<cmdWordSize; i++)
		{
			Utils.assert_(ser.readInt () == i, "Failed to unserialize CWD on " + i);
			commandWordDefs.addElement(unserializeCommandWord(ser));
			Utils.assert_(ser.readString ().equals("" + i), "Failed to unserialize CWD on " + i);
			
		}
	}

	public void serialize(ISerializer ser) {
		eventScheduler.serialize(ser);
		
		ser.writeDict(usedCodes);
		
		ser.writeInt(commandWordDefs.size());
		
		for (int i =0; i<commandWordDefs.size(); i++)
		{
			ser.writeInt(i);
			CommandWordDefBase cmdDef = (CommandWordDefBase) commandWordDefs.elementAt(i);

			cmdDef.serialize(ser);
			ser.writeInt(i);
			
		}
	}
	 	
	public Event createProcessByName(String name)
	{
		Event[] process = new Event[] {
				new DoNothingEvent(this)
		};
		for (int i = 0; i < process.length; i++)
		{
			if (process[i].getName().equals(name))
			{
				return process[i];
			}
		}
		return null;
	}

	public int randomInt(int n) {
		long x = random.nextLong();
		if (x < 0) x = -x;
		if (x < 0) x = 0; // it is possible for minint
		return (int) (x%n);
	}

	private void serializeProcess(ISerializer ser, IEvent item) {
		ser.writeString(item.getName());
		ser.writeString("PR_DATA");
		item.serializeData(ser);
	}
	
	private Event unserializeProcess(ISerializer ser) {
		String name = ser.readString();
		Event process = createProcessByName(name);
		Utils.assert_(process != null, "Failed to unserialize process: " + name);
		Utils.assert_(ser.readString().equals("PR_DATA"), "Failed to read process header on " + name);
		process.unserializeData(ser);
		return process;
	}
	
}
