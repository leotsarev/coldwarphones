package ru.rpg.phones.process;

import ru.rpg.phones.InteractionModel.Descriptor;
import ru.rpg.phones.process.ProcessModelBase.Event;

public class DoNothingEvent extends Event {

	public DoNothingEvent(ProcessModelBase model) {
		super(model);
	}

	public Descriptor handle() {
		// NOP
		return null;
	}

	public String getName() {
		return "DoNothing";
	}

}
