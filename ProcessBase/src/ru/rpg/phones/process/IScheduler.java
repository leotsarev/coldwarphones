package ru.rpg.phones.process;

public interface IScheduler {
	void afterSecs(IEvent process, int secs);
	void afterMins(IEvent event, int mins);
	void now (IEvent event);

}
