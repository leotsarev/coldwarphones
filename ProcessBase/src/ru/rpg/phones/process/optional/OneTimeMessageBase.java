package ru.rpg.phones.process.optional;

import ru.rpg.phones.InteractionModel.*;
import ru.rpg.phones.process.ProcessModelBase;


public abstract class OneTimeMessageBase extends ProcessModelBase.Event {
	public OneTimeMessageBase(ProcessModelBase model, String message) {
		super(model);
		setStringArg("message", message);
	}

	public Descriptor handle() {
		return createMessage(getStringArg("message"));
	}

}
