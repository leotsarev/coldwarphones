package ru.rpg.phones.process.optional;

import ru.rpg.phones.InteractionModel.*;
import ru.rpg.phones.process.ProcessModelBase;


public abstract class MenuBase extends ProcessModelBase.Event {

	public MenuBase(ProcessModelBase model) {
		super(model);
		// TODO Auto-generated constructor stub
	}

	public Descriptor handle() {
		MenuDescriptor menu = new MenuDescriptor();
		addMenuItems(menu);
		menu.timeoutCommand = getTimeoutCommandWord();
		menu.timeout = getMenuTimeout();
		menu.menuHeader = getMenuName();
		return menu;
	}

	public String getTimeoutCommandWord() {
		return "";
	}

	public int getMenuTimeout() {
		return 30;
	}

	public String getMenuName() {
		return "Меню";
	}

	public abstract void addMenuItems(MenuDescriptor menu);

	protected void addClose(MenuDescriptor menu) {
		menu.addItem("Закрыть", "");
	}
	
	public final void cleanup()
	{
	}

}
